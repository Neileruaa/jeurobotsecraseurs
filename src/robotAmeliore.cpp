#include "robotAmeliore.h"
#include "joueur.h"
#include "personne.h"
robotAmeliore::robotAmeliore(int x, int y, terrain&t, int element):personne {x, y, t, element}
{}

/**
 * Approche le robot amélioré du joueur j.
 * Passe par le chemin le plus court
 *
 * @param j un joueur
 */
void robotAmeliore::approcheJoueur(const joueur&j)
{
    if(x()>j.x())
    {
        if(y()>j.y())
        {
            deplaceHaut();
            deplaceGauche();
        }
        else if(y()<j.y())
        {
            deplaceBas();
            deplaceGauche();
        }
        else{
            deplaceGauche();
        }
    }
    else if (x()<j.x())
    {
        if(y()>j.y())
        {
            deplaceHaut();
            deplaceDroite();
        }
        else if(y()<j.y())
        {
            deplaceBas();
            deplaceDroite();
        }
        else{
            deplaceDroite();
        }
    }
    else{
        if(y()>j.y())
        {
            deplaceHaut();
        }
        else if(y()<j.y())
        {
            deplaceBas();
        }
    }


}
