#include "doctest.h"
#include "robotAmeliore.h"
#include "terrain.h"
#include "joueur.h"
#include "personne.h"
#include <iostream>
#include <sstream>
//
// Created by antoine on 09/11/2019.
//

using namespace std;

TEST_CASE("Test de la création d'un robot") {
    terrain t{4,4};
    robotAmeliore r{0,0,t,2};
    CHECK(r.x()==0);
    CHECK(r.y()==0);
    CHECK(r.element()== 2);
}

TEST_CASE("Test de l'approche du robot amélioré vers un joueur") {
    terrain t{6,6};
    robotAmeliore r{0,0,t,4};
    r.placeTerrain();
    joueur j{2,1,t,3};
    j.placeTerrain();

    r.approcheJoueur(j);
    CHECK(r.x()==1);
    CHECK(r.y()==1);

    r.approcheJoueur(j);
    CHECK(r.x()==2);
    CHECK(r.y()==1);
}
