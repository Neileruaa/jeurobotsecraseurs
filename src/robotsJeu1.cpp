#include "robotsJeu1.h"
#include <ctime>
#include "joueur.h"
#include "personne.h"
#include "robotAmeliore.h"

robotsJeu1::robotsJeu1(int nbRobot, int nbRobotsAmel, terrain& t):d_t{t}
{
    d_personnes.reserve((nbRobot+nbRobotsAmel)*2);
    srand(time(NULL));
    for(int i=0; i<nbRobot; i++)
    {
        robot* r1= new robot(rand()%t.x(),rand()%t.y(), t, ROBOT);
        d_personnes.push_back(r1);
    }
    for(int i=0; i<nbRobotsAmel; i++)
    {
        robotAmeliore* r1= new robotAmeliore(rand()%t.x(),rand()%t.y(), t, ROBOTAMELIORE);
        d_personnes.push_back(r1);
    }
}

robotsJeu1::~robotsJeu1()
{
    //dtor
}

bool robotsJeu1::testerCollision(const joueur&joueur1)
{
    bool collision=false;
    bool collisionJoueur=false;
    int newX, newY;
    for(unsigned int i=0; i<d_personnes.size()&&!collisionJoueur; i++)
    {
        collisionJoueur=d_personnes[i]->touche(joueur1);
        if(d_personnes[i]->element()!=DEBRIS)
        {
            for(unsigned int j=i+1; j<d_personnes.size()&&!collisionJoueur; j++)
            {
                collision=d_personnes[i]->touche(*d_personnes[j]);
                if(collision)
                {
                    if(d_personnes[j]->element()!=DEBRIS)
                    {
                        newX=d_personnes[j]->x();
                        newY=d_personnes[j]->y();
                        delete d_personnes[j];
                        d_personnes[j]=d_personnes[d_personnes.size()-1];
                        d_personnes[d_personnes.size()-1]=nullptr;
                        d_personnes.pop_back();
                        debris *db=new debris(newX, newY, d_t);
                        d_personnes.push_back(db);
                    }
                    delete d_personnes[i];
                    d_personnes[i]=d_personnes[d_personnes.size()-1];
                    d_personnes[d_personnes.size()-1]=nullptr;
                    d_personnes.pop_back();
                }
            }
        }
    }
    return collisionJoueur;
}

void robotsJeu1::avancerRobots(const joueur &j)
{
    for(unsigned int i=0; i<d_personnes.size(); i++)
    {
        if(d_personnes[i]->element()!=DEBRIS)
        {
            d_personnes[i]->approcheJoueur(j);
        }
    }
}
