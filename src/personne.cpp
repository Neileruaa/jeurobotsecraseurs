#include "personne.h"
#include "terrain.h"

/**
* Permet de créer une personne grace aux coordonnées x et y sur un terrain T avec un element.
*/
personne::personne(int x, int y, terrain&t, int element):d_x{x}, d_y{y}, d_t{t}, d_element{element}
{
}

/**
* Permet de créer une personne grace au constructeur vide
*/
personne::~personne()
{
    d_t.ajouteElement(d_x, d_y, 0);
}

/**
* Ajoute l'élement courant ainsi que les coordonnées dans la classe personne
*/
void personne::placeTerrain()
{
    d_t.ajouteElement(d_x, d_y , d_element);
}

/*
* Permet de se déplacer d'une case sur la gauche
*/
void personne::deplaceGauche()
{
    int element=d_t.renvoiElement(d_x,d_y);
    d_t.ajouteElement(d_x,d_y,element-d_element);
    //Deplace l'élement d'une case sur la gauche (axe x)
    d_x--;
    if(d_x<0)
    {
        int yTmp;
        d_t.tailleTerrain(d_x,yTmp);
        d_x--;
    }
    int element2=d_t.renvoiElement(d_x,d_y);
    d_t.ajouteElement(d_x,d_y,element2+d_element);
}

/**
* Permet de se déplacer d'une case sur la droite
*/
void personne::deplaceDroite()
{
    int element=d_t.renvoiElement(d_x,d_y);
    d_t.ajouteElement(d_x,d_y,element-d_element);
    //!Deplace l'élement d'une case sur la droite (axe x)
    d_x++;
    int xtmp,ytmp;
    d_t.tailleTerrain(xtmp, ytmp);
    if(d_x>=xtmp)
    {
        d_x=0;
    }
    int element2=d_t.renvoiElement(d_x,d_y);
    d_t.ajouteElement(d_x,d_y,element2+d_element);
}

/**
* Permet de se déplacer d'une case sur le haut
*/
void personne::deplaceHaut()
{
    int element=d_t.renvoiElement(d_x,d_y);
    d_t.ajouteElement(d_x,d_y,element-d_element);
    //!Deplace l'élement d'une case sur la droite (axe y)
    d_y--;
    if(d_y<0)
    {
        int xTmp;
        d_t.tailleTerrain(xTmp,d_y);
        d_y--;
    }
    int element2=d_t.renvoiElement(d_x,d_y);
    d_t.ajouteElement(d_x,d_y,element2+d_element);
}


/**
* Permet de se déplacer d'une case sur la bas
*/
void personne::deplaceBas()
{
    int element=d_t.renvoiElement(d_x,d_y);
    d_t.ajouteElement(d_x,d_y,element-d_element);
    //Deplace l'élement d'une case sur la droite (axe y)
    d_y++;
    int xtmp,ytmp;
    d_t.tailleTerrain(xtmp, ytmp);
    if(d_y>=ytmp)
    {
        d_y=0;
    }
    int element2=d_t.renvoiElement(d_x,d_y);
    d_t.ajouteElement(d_x,d_y,element2+d_element);
}

int personne::x() const
{
    return d_x;
}
int personne::y() const
{
    return d_y;
}
int personne::element() const
{
    return d_element;
}

bool personne::touche(const personne& p) const
{
    return (d_x==p.x() && d_y==p.y());
}

terrain& personne::ter()
{
    return d_t;
}

void personne::approcheJoueur(const joueur&j)
{

}


