#include "terrain.h"
#include <vector>
#include <fstream>
#include <string>
terrain::terrain(int taille): d_terter{static_cast<unsigned int>(taille),std::vector<int>(static_cast<unsigned int> (taille), 0)}, d_x{taille}, d_y{taille}
{
    //ctor
}

terrain::terrain(int x, int y): d_terter{static_cast<unsigned int>(x),std::vector<int>(static_cast<unsigned int> (y), 0)}, d_x{x}, d_y{y}
{
    //ctor
}
terrain::terrain():d_terter{static_cast<unsigned int>(0),std::vector<int>(static_cast<unsigned int> (0), 0)}
{
}

terrain::~terrain()
{
    //dtor
}

/*
* Ajoute un élément au coordonnées x et y
*/
void terrain::ajouteElement(int x, int y, int element)
{
    d_terter[x][y]=element;
}

/**
* Renvoi l'élément au coordonnées x et y
*/
int terrain::renvoiElement(int x, int y) const
{
    return d_terter[x][y];
}

/*
* Renvoi le nombre d'élement sur le terrain
*/
int terrain::nombreElement(int element) const
{
    int nb=0;
    for(unsigned int i=0;i<d_terter.size();i++)
    {
        for(unsigned int j=0;j<d_terter[j].size();j++)
        {
            if(d_terter[i][j]==element)
                nb++;
        }
    }
    return nb;
}

/*
* Fixe la taille du terrain
*/
void terrain::tailleTerrain(int&x, int&y) const
{
    x=d_terter.size();
    y=d_terter[0].size();

}

/*
* Permet de sauvegarder le terrain dans un fichier
*/
void terrain::sauver(const std::string& nomFichier) const
{
    std::ofstream fOUT{nomFichier+".txt"};
    if(!fOUT)
    {
        throw std::string("Le fichier ne peut etre cree");
    }
    else{
        fOUT<<d_terter.size()<<" "<<d_terter[0].size()<<std::endl;
        for(unsigned int i=0;i<d_terter.size();i++)
        {
            for(unsigned int j=0; j<d_terter[i].size();j++)
            {
                fOUT<<d_terter[i][j]<<" ";
            }
        }
    }
}

/*
* Permet de charger le terrain depuis un fichier
*/
bool terrain::charger(const std::string &nomFichier)
{
    std::ifstream fIN{nomFichier+".txt"};
    if(!fIN)
    {
        throw std::string("Le fichier est introuvable");
        return false;
    }
    else
    {
            int x,y;
        fIN>>x>>y;
        d_terter.resize(x, std::vector<int>(y));
        for(unsigned int i=0;i<d_terter.size();i++)
        {
            for(unsigned int j=0; j<d_terter[0].size();j++)
            {
                fIN>>d_terter[i][j];
            }
        }
    }
    return true;
}

/*
* Permet de vider le terrain
*/
void terrain::vide()
{
    for(int i=0;i<d_x; i++)
    {
        for(int j=0; j<d_y; j++)
        {
            d_terter[i][j]=0;
        }
    }
}

int terrain::x() const
{
    return d_x;
}
int terrain::y() const
{
    return d_y;
}
