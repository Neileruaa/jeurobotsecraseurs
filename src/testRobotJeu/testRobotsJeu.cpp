#include "doctest.h"
#include "robotAmeliore.h"
#include "robot.h"
#include "robotsJeu.h"
#include "terrain.h"
#include "debris.h"
#include <iostream>
#include <sstream>
//
// Created by antoine on 14/12/2019.
//

TEST_CASE("Test de la création d'un jeu de robots") {
    terrain t{6,6};
    robotsJeu rj{1,1,t};

    CHECK(t.nombreElement(2)==1);
    CHECK(t.nombreElement(4)==1);
    CHECK(rj.personnes().size()==2);
}

TEST_CASE("Test de la création d'un jeu de robots à partir d'un terrain") {
    terrain t{6,6};
    robotAmeliore r1{0,0,t,4};
    robotAmeliore r2{1,1,t,4};
    robot r3{2,2,t,2};
    r1.placeTerrain();
    r2.placeTerrain();
    r3.placeTerrain();

    robotsJeu rj{t};

    CHECK(t.nombreElement(2)==1);
    CHECK(t.nombreElement(4)==2);
    CHECK(rj.personnes().size()==3);
}

TEST_CASE("Test de la phase d'avancement des robots"){
    terrain t{6,6};
    robot r{0,0,t,2};
    r.placeTerrain();
    joueur j{2,0,t,3};
    j.placeTerrain();

    robotsJeu rj{t};

    rj.avancerRobots(j);
    CHECK(r.x()==1);
    CHECK(r.y()==0);
}

TEST_CASE("Test la collision entre un robot et le joueur"){
    terrain t{6,6};
    robot r{0,0,t,2};
    r.placeTerrain();
    joueur j{1,0,t,3};
    j.placeTerrain();

    robotsJeu rj{t};

    CHECK(!rj.testerCollision(j));
    CHECK(t.nombreElement(3)==1);
    rj.avancerRobots(j);
    CHECK(rj.testerCollision(j));
    CHECK(t.nombreElement(3)==0);
}

TEST_CASE("Test la collision entre un robot et un autre robot"){
    terrain t{6,6};
    robotAmeliore r{0,0,t,2};
    robot r2{0,1,t,2};
    r.placeTerrain();
    r2.placeTerrain();
    joueur j{1,2,t,3};
    j.placeTerrain();

    robotsJeu rj{t};

    CHECK(t.nombreElement(1)==1);
    CHECK(t.nombreElement(2)==1);
    CHECK(t.nombreElement(4)==1);
    rj.avancerRobots(j);
    CHECK(!rj.testerCollision(j);
    CHECK(t.nombreElement(1)==1);
    CHECK(t.nombreElement(2)==0);
    CHECK(t.nombreElement(4)==0);
    CHECK(t.nombreElement(3)==1);
}

TEST_CASE("Test la collision entre un robot et un débris"){
    terrain t{6,6};
    robotAmeliore r{0,0,t,2};
    robot r2{0,1,t,2};
    r.placeTerrain();
    r2.placeTerrain();
    joueur j{1,2,t,3};
    j.placeTerrain();

    robotsJeu rj{t};
    rj.avancerRobots(j);

    robotAmeliore r3{0,0,t,2};
    r3.placeTerrain();

    rj.avancerRobots(j);
    CHECK(!rj.testerCollision(j));
    CHECK(t.nombreElement(1)==1);
    CHECK(t.nombreElement(2)==0);
    CHECK(t.nombreElement(4)==0);
    CHECK(t.nombreElement(3)==1);
}
