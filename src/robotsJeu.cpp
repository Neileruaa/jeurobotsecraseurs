#include "robotsJeu.h"
#include <ctime>
#include "joueur.h"
#include "personne.h"
#include "robotAmeliore.h"

/**
 * Créer un jeu de robots en ajoutant un certains nombre de robots et robots améliorés.
 *
 * @param nbRobot
 * @param nbRobotsAmel
 * @param t
 */
robotsJeu::robotsJeu(int nbRobot, int nbRobotsAmel, terrain& t):d_t{t}
{
    d_personnes.resize(0);
    d_personnes.reserve((nbRobot+nbRobotsAmel)*2);
    srand(time(NULL));
    for(int i=0; i<nbRobot; i++)
    {
        robot* r1= new robot(rand()%t.x(),rand()%t.y(), t, ROBOT);
        r1->placeTerrain();
        d_personnes.push_back(r1);
    }
    for(int i=0; i<nbRobotsAmel; i++)
    {
        robotAmeliore* r1= new robotAmeliore(rand()%t.x(),rand()%t.y(), t, ROBOTAMELIORE);
        r1->placeTerrain();
        d_personnes.push_back(r1);
    }
}

robotsJeu::robotsJeu(terrain& t):d_t{t}
{
    d_personnes.resize(0);
    int x,y;
    d_t.tailleTerrain(x,y);
    d_personnes.reserve(x+y);
    for(int i=0;i<x;i++)
    {
        for(int j=0;j<y;j++)
        {
            int elem=d_t.renvoiElement(x, y);
            if(elem!=0)
            {
                personne*p=new personne(i,j,t,elem);
                d_personnes.push_back(p);
            }
        }
    }
}


robotsJeu::~robotsJeu()
{
    //dtor
}

/**
 * Vérifie les collisions entre les robots et les différentes entités (robot, robot amélioré, joueur et débris)
 *
 * @param joueur1
 * @return (true si le robot entre en collision avec un joueur sinon false)
 */
bool robotsJeu::testerCollision(const joueur&joueur1)
{
    bool collision=false;
    bool collisionJoueur=false;
    int newX, newY;
    for(unsigned int i=0; i<d_personnes.size()&&!collisionJoueur; i++)
    {
        collisionJoueur=d_personnes[i]->touche(joueur1); //!vérifie si l'élément courant est aux mêmes coordonnées que le joueur
        if(d_personnes[i]->element()!=DEBRIS)
        {
            for(unsigned int j=i+1; j<d_personnes.size()&&!collisionJoueur; j++)
            {
                collision=d_personnes[i]->touche(*d_personnes[j]); //!vérifie si les 2 éléments i et j sont aux mêmes coordonnées
                if(collision)
                {
                    if(d_personnes[j]->element()!=DEBRIS) //!si ce sont 2 robots qui entrent en collision, on les supprime et on créer un débris à la place de l'endroit de la collision
                    {
                        newX=d_personnes[j]->x();
                        newY=d_personnes[j]->y();
                        delete d_personnes[j];
                        d_personnes[j]=d_personnes[d_personnes.size()-1];
                        d_personnes[d_personnes.size()-1]=nullptr;
                        d_personnes.pop_back();
                        debris *db=new debris(newX, newY, d_t);
                        d_personnes.push_back(db);
                        db->placeTerrain();
                    }
                    delete d_personnes[i]; //!si le robot i entre en collision avec un débris alors on le supprime
                    d_personnes[i]=d_personnes[d_personnes.size()-1];
                    d_personnes[d_personnes.size()-1]=nullptr;
                    d_personnes.pop_back();
                }
            }
        }
    }
    if(collisionJoueur)
    {
        d_t.ajouteElement(joueur1.x(), joueur1.y(), 0);
    }
    return collisionJoueur;
}

/**
 * Fait avancer tous les robots de la partie vers le joueur j?
 *
 * @param j
 */
void robotsJeu::avancerRobots(const joueur &j) //fait avancer tous les robots vers le joueur
{
    for(unsigned int i=0; i<d_personnes.size(); i++)
    {
        if(d_personnes[i]->element()!=DEBRIS)
        {
            d_personnes[i]->approcheJoueur(j);
        }
    }
}

/**
 * Retourne la liste des robots.
 *
 * @return vector<personne*> la liste des robots
 */
std::vector<personne*> robotsJeu::personnes() const
{
    return d_personnes;
}
