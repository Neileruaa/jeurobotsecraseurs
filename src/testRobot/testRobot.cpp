#include "../../include/doctest.h"
#include "../../include/robot.h"
#include "../../include/terrain.h"
#include "../../include/joueur.h"
#include <iostream>
#include <sstream>
//
// Created by antoine on 09/11/2019.
//

using namespace std;

TEST_CASE("Test de la création d'un robot") {
    terrain t{4,4};
    robot r{0,0,t,2};
    CHECK(r.x()==0);
    CHECK(r.y()==0);
    CHECK(r.element()==2);
}

TEST_CASE("Test de l'approche du robot vers un joueur") {
    terrain t{6,6};
    robot r{0,0,t,2};
    r.placeTerrain();
    joueur j{2,1,t,3};
    j.placeTerrain();

    r.approcheJoueur(j);
    CHECK(r.x()==1);
    CHECK(r.y()==0);

    r.approcheJoueur(j);
    CHECK(r.x()==2);
    CHECK(r.y()==0);

    r.approcheJoueur(j);
    CHECK(r.x()==2);
    CHECK(r.y()==1);
}
