#include "doctest.h"
#include "robot.h"
#include "terrain.h"
#include "joueur.h"
#include <iostream>
#include <sstream>

using namespace std;

TEST_CASE("Test de la création d'un terrain") {
    terrain t{4};
    CHECK(t.x()==4);
    CHECK(t.y()==4);

    terrain t1{4, 4};
    CHECK(t1.x()==4);
    CHECK(t1.y()==4);

    terrain t2{0, 0};
    CHECK(t2.x()==4);
    CHECK(t2.y()==0);
}

TEST_CASE("Test l'ajout d'un element sur un terrain") {
    terrain t{4};
    int elem = 1;
    int testX = 1;
    int testY = 1;
    t.ajouteElement(testX, testY, elem);
    CHECK(t.renvoiElement(testX, testY) == elem);
}

TEST_CASE("Test nombreElement qui compte le nombre d'element donné dans un terrain") {
    terrain t{4};
    int elem = 1;
    int testX = 1;
    int testY = 1;
    t.ajouteElement(testX, testY, elem);
    CHECK(t.nombreElement(elem) == 1);
}

TEST_CASE("Test la méthode qui vide le plateau") {
    terrain t{4};
    int elem = 1;
    int testX = 1;
    int testY = 1;
    t.ajouteElement(testX, testY, elem);
    t.vide();
    CHECK(t.nombreElement(elem) == 0);
}

