#include "doctest.h"
#include "robot.h"
#include "terrain.h"
#include "joueur.h"
#include <iostream>
#include <sstream>

using namespace std;

TEST_CASE("Test de la création d'une personne") {
    terrain t{4,4};
    const int JOUEUR=3;
    const int testX = 1;
    const int testY = 1;
    personne p{testX, testY, t, JOUEUR};
    CHECK(p.x() == testX);
    CHECK(p.y() == testY);
    CHECK(p.ter() == t);
    CHECK(p.element() == JOUEUR);
}

TEST_CASE("Test de déplacement d'une personne") {
    terrain t{4,4};
    const int JOUEUR=3;
    const int testX = 1;
    const int testY = 1;
    personne p{testX, testY, t, JOUEUR};

    p.deplaceGauche();
    CHECK(p.x() == testX-1);
    CHECK(p.y() == testY);

    p.deplaceDroite();
    CHECK(p.x() == testX+1);
    CHECK(p.y() == testY);

    p.deplaceBas();
    CHECK(p.x() == testX);
    CHECK(p.y() == testY + 1);

    p.deplaceHaut();
    CHECK(p.x() == testX);
    CHECK(p.y() == testY-1);
}

TEST_CASE("Test pour savoir si une personne est touchée") {
    terrain t{4,4};
    const int JOUEUR=3;
    const int testX = 1;
    const int testY = 1;
    personne p{testX, testY, t, JOUEUR};


    const int testX2 = 1;
    const int testY2 = 1;
    personne p2{testX2, testY2, t, JOUEUR};

    CHECK(p.touche(p2) == true);
}
