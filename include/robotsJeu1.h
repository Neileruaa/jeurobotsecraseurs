#ifndef ROBOTSJEU1_H
#define ROBOTSJEU1_H
#include "terrain.h"
#include <vector>
#include "robot.h"
#include "debris.h"
#include "robotAmeliore.h"

class robotsJeu1
{
    public:
        robotsJeu1(int nbRobots,int nbRobotsAmel, terrain&t);
        virtual ~robotsJeu1();
        bool testerCollision(const joueur&j);
        void avancerRobots(const joueur &j);

    protected:

    private:
        std::vector<personne*> d_personnes;
        terrain &d_t;
};

#endif // ROBOTSJEU1_H
