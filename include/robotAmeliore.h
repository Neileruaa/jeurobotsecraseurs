#ifndef ROBOTAMELIORE_H
#define ROBOTAMELIORE_H

#include "personne.h"

class terrain;

class joueur;

class robotAmeliore : public personne {
public:
    robotAmeliore(int x, int y, terrain &t, int element);

    virtual void approcheJoueur(const joueur &j) override;

private:
    int d_element = ROBOTAMELIORE;
};

#endif // ROBOTAMELIORE_H
