#ifndef JEU_H
#define JEU_H
#include "joueur.h"
#include "robot.h"
#include "terrain.h"
#include <vector>
#include "robotsJeu.h"
class jeu
{
    public:
        jeu(int nbRobot, int nbRobotsAmel,terrain &t, int tailleAffichage);
        jeu(terrain &t, int tailleAffichage);
        virtual ~jeu();
        bool lancer();
        void afficherTerrain();
        void finJeu();
    private:
        joueur d_joueur;
        robotsJeu robots;
        terrain& d_t;
        int d_tailleAffiche;
};

#endif // JEU_H

