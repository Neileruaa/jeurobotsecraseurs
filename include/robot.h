#ifndef ROBOT_H
#define ROBOT_H
#include "personne.h"
#include "joueur.h"

class robot: public personne
{
    public:
        robot(int x, int y, terrain&t, int element);
        virtual void approcheJoueur(const joueur&j) override;
    private:
        int d_element = ROBOT;
};

#endif // ROBOT_H
