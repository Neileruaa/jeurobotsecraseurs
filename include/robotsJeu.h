#ifndef ROBOTSJEU1_H
#define ROBOTSJEU1_H
#include "terrain.h"
#include <vector>
#include "robot.h"
#include "debris.h"
#include "robotAmeliore.h"
class joueur;

class robotsJeu
{
    public:
        robotsJeu(int nbRobots,int nbRobotsAmel, terrain&t);
        robotsJeu(terrain& t);
        virtual ~robotsJeu();
        bool testerCollision(const joueur&j);
        void avancerRobots(const joueur &j);
        std::vector<personne*> personnes() const;

    protected:

    private:
        std::vector<personne*> d_personnes;
        terrain &d_t;
};

#endif // ROBOTSJEU1_H
