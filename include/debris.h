#ifndef DEBRIS_H
#define DEBRIS_H
#include "personne.h"

/**La classe debris herite de la classe personne*/
class debris : public personne
{
    public:
        debris(personne&p);
        debris(int x, int y, terrain&t);
        virtual ~debris();
        virtual void approcheJoueur(const joueur&j);
    private:
        /**Definis que la personne est un debris*/
        int element=DEBRIS;
};

#endif // DEBRIS_H
