#ifndef TERRAIN_H
#define TERRAIN_H
#include <vector>
#include <string>
class terrain
{
    public:
        terrain(int taille);
        terrain();
        terrain(int x, int y);
        ~terrain();
        void ajouteElement(int x, int y, int element);
        int renvoiElement(int x, int y) const;
        int nombreElement(int element) const;
        void tailleTerrain(int&x, int&y) const;
        void sauver(const std::string& nomFichier = "sauveTerrain.txt") const;
        bool charger(const std::string& nomFichier = "sauveTerrain.txt");
        void vide();
        int x() const;
        int y() const;
    private:
        std::vector<std::vector<int>> d_terter;
        int d_x;
        int d_y;

};

#endif // TERRAIN_H
