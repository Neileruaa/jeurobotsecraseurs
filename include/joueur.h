#ifndef JOUEUR_H
#define JOUEUR_H
#include "personne.h"

/**La classe joueur herite de la classe personne*/
class joueur: public personne
{
    public:
        /**Constructeur de joueur avec sa position (x,y),le terrain sur lequel il est et element = joueur*/
        joueur(int x, int y, terrain&t, int element);
        /**Constructeur */
        /**Fais avancer le joueur*/
        bool avance(const char&resultat);

    private:
        /**Definis que la personne est un joueur*/
        int element = JOUEUR;
};

#endif // JOUEUR_H
