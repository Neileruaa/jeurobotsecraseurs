#ifndef PERSONNE_H
#define PERSONNE_H
#include "terrain.h"
const int JOUEUR=3;
const int ROBOT=2;
const int ROBOTAMELIORE=4;
const int DEBRIS=1;
class joueur;
class personne
{
    public:
        // Constructeurs
        personne(int x, int y, terrain&t, int element);
        virtual ~personne();
        //Place la personne sur le terrain
        void placeTerrain();

        //Méthodes permettant de se déplacer

        void deplaceBas();
        void deplaceHaut();
        void deplaceDroite();
        void deplaceGauche();
        virtual void approcheJoueur(const joueur&j);
        // Accesseur de d_x
        int x() const;
        // Accesseur de d_y
        int y() const;
        // Accesseur de d_element
        int element() const;
        // Accesseur de d_t
        terrain &ter();
        // Méthodes permettant de savoir si une personne est touchée
        bool touche(const personne& p) const;
        //Surcharge de l'opérateur = pour vérifier que 2 personnes sont égales
        personne &operator=(personne &p);
    private:
        // Coordonnées de la personne en x et y
        int d_x, d_y;
        // Le terrain auquel elle appartient
        terrain& d_t;
        // Le type d'element a laquelle elle correspond (JOUEUR, ROBOT, ROBOTAMELIORE, DEBRIS)
        int d_element;
};

#endif // PERSONNE_H
